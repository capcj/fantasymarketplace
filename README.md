# fantasymarketplace

A dev challenge

## Usage
```
docker-compose up
```
- Copy your pagarme.json with the required keys to API integration into src/assets/data directory;

pagarme.json structure:

```
{
  "key": {
    "api": "api_key_string",
      "encryption": "encryption_key_string"
  }
}
```

- Access in your http://localhost:8080
