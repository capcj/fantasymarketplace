<?php
namespace Classes;

final class Cart
{
  private $products;

  public static function build(array $products, Session $session): Cart
  {
    return $session->contains('cart') ?
      $session->get('cart') : new Cart($products);
  }

  private function __construct(array $products)
  {
    $this->products = [];
    $this->products($products);
  }

  public function products(): array
  {
    return $this->products;
  }

  public function totalProducts(): int
  {
    $count = function ($sum, $product) {
      $sum += $product->amount(null);
      return $sum;
    };

    return count($this->products) > 0 ?
      array_reduce($this->products, $count) : 0;
  }

  public function totalValue(bool $format = false)
  {
    $sum = function ($sum, $product) {
      $sum += $product->totalValue();
      return $sum;
    };

    if (count($this->products) > 0) {
      $result = array_reduce($this->products, $sum);
    } else {
      $result = 0;
    }

    return $format ?
      Marketplace::formatNumber($result) :
      $result;
  }

  public function add(Product $product): Cart
  {
    $productId = $product->id(null);
    if (isset($this->products[$productId])) {
      $this->products[$productId]->increment();
    } else {
      $this->products[$productId] = $product;
    }

    return $this;
  }

  public function remove(int $productId): Cart
  {
    unset($this->products[$productId]);

    return $this;
  }

  public function recalc(array $productArr): Cart
  {
    foreach ($productArr as $productId => $amount) {
      if (isset($this->products[$productId])) {
        $this->products[$productId]->amount($amount);
      }
    }

    return $this;
  }

  public function clear(): Cart
  {
    $this->products = [];
    return $this;
  }

  public function providers(): array
  {
    $providers = [];
    foreach ($this->products as $product) {
      if (!isset($providers[$product->provider()->id()])) {
        $providers[$product->provider()->id()] = $product->provider();
      }
    }

    //Owner comes first
    $isOwner = function (\Classes\Provider $provider, \Classes\Provider $provider2) {
      return $provider->owner() > $provider2->owner() ? -1 : 1;
    };

    uasort($providers, $isOwner);

    return $providers;
  }

  public function valuesPerProviderRecipient(): array
  {
    $valuesPerProvider = [];
    foreach ($this->products as $product) {
      $valuesPerProvider[$product->provider()->recipient()] += $product->totalValue();
    }

    return $valuesPerProvider;
  }
}
