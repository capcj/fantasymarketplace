<?php
namespace Classes;

interface DAOInterface
{
    public function get(string $table, array $filter): array;
}
