<?php
namespace Classes;

class DAOJson implements DAOInterface
{
    private $content;

    public function __construct(string $filename)
    {
        if (empty($filename)) {
            return [];
        }
        $this->content = file_get_contents("assets/data/{$filename}.json");
    }

    public function get(string $table, array $filter): array
    {
        $mainClass = '\\Classes\\' . ucfirst($table);
        $data = json_decode($this->content, true)[$table];
        $parsedData = [];
        if (count($filter) > 0) {
            foreach ($data as $item) {
                if (in_array($item['id'], $filter)) {
                    $parsedData[] = new $mainClass($item);
                }
            }
        } else {
            if (is_array(reset($data))) {
                foreach ($data as $item) {
                    $parsedData[] = new $mainClass($item);
                }
            } else {
                $parsedData = [new $mainClass($data)];
            }
        }
        return $parsedData;
    }
}
