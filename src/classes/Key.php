<?php
namespace Classes;

class Key
{
    private $api;
    private $encryption;

    public function __construct(array $keys)
    {
        foreach ($keys as $field => $value) {
            $this->{$field}($value);
        }
    }

    public function api($newApi = null): string
    {
        if (is_string($newApi) && $newApi != '') {
            $this->api = $newApi;
        }
        return $this->api;
    }

    public function encryption($newEncryption = null): string
    {
        if (is_string($newEncryption) && $newEncryption != '') {
            $this->encryption = $newEncryption;
        }
        return $this->encryption;
    }
}
