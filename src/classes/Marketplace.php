<?php
namespace Classes;

class Marketplace
{
    public const FARE = 42;
    private $dao;

    public function __construct()
    {
        $this->dao = new DAOJson('marketplace');
    }

    public function getProducts(array $filter): array
    {
        return $this->dao->get('product', $filter);
    }

    public function getOwner()
    {
        return $this->dao->get('product', [1])[0];
    }

    /*
     * 0 => Partner Rule int,
     * 1 => Owner Rule int
     *
     */

    public function getSplitRules(): array
    {
        return [
      function (int $value, int $fare): array {
          $ownerPercentage = 15;
          $partnerPercentage = 85;
          $ownerValue = (int) (($ownerPercentage/100) * $value);
          $recipientValue = (int) ($value - $ownerValue) + $fare;
          return [
          $recipientValue,
          $ownerValue
        ];
      },
      function (int $value, int $fare): int {
          return $value + $fare;
      }
    ];
    }

    public function getOwnerRecipient(\Classes\Cart $cart): string
    {
        foreach ($cart->providers() as $provider) {
            if ($provider->owner() === 1) {
                return $provider->recipient();
            }
        }

        return $this->getOwner()->provider()->recipient();
    }

    final public static function formatNumber(int $num): string
    {
        return 'R$ ' . number_format($num, 2, ',', '.');
    }
}
