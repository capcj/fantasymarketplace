<?php
namespace Classes;

class Product
{
    private $id;
    private $name;
    private $category;
    private $provider;
    private $value;
    private $amount;
    public function __construct(array $product)
    {
        if (!isset($product['amount'])) {
            $product['amount'] = 1;
        }
        foreach ($product as $field => $value) {
            $this->{$field}($value);
        }
    }

    public function id($newId = null)
    {
        if (is_int($newId) && $newId > 0) {
            $this->id = $newId;
        }
        return $this->id;
    }

    public function name($newName = null)
    {
        if (is_string($newName) && $newName != '') {
            $this->name = $newName;
        }
        return $this->name;
    }

    public function category($newCategory = null)
    {
        if (is_string($newCategory) && $newCategory != '') {
            $this->category = $newCategory;
        }
        return $this->category;
    }

    public function provider($newProvider = null)
    {
        if (is_array($newProvider) && count($newProvider) > 0) {
            $this->provider = new Provider($newProvider);
        } elseif (is_a($newName, 'Provider')) {
            $this->provider = $newProvider;
        }
        return $this->provider;
    }

    public function value($newValue = null, bool $format = false)
    {
        if (is_numeric($newValue)) {
            $this->value = (int) $newValue;
        }
        return $format ?
      Marketplace::formatNumber($this->value)
      : $this->value;
    }

    public function amount($newAmount = null)
    {
        if (is_numeric($newAmount)) {
            $this->amount = (int) $newAmount;
        }
        return $this->amount;
    }

    public function increment(): int
    {
        return ++$this->amount;
    }

    public function totalValue()
    {
        return $this->amount(null) * $this->value(null);
    }
}
