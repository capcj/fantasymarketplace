<?php
namespace Classes;

class Provider
{
    private $id;
    private $name;
    private $owner;
    private $recipient;
    public function __construct(array $provider)
    {
        foreach ($provider as $field => $value) {
            $this->{$field}($value);
        }
    }

    public function id($newId = null): int
    {
        if (is_int($newId) && $newId > 0) {
            $this->id = $newId;
        }
        return $this->id;
    }

    public function name($newName = null): string
    {
        if (is_string($newName) && $newName != '') {
            $this->name = $newName;
        }
        return $this->name;
    }

    public function owner($newStatus = null): int
    {
        if (is_numeric($newStatus)) {
            $this->owner = (int) $newStatus;
        }
        return $this->owner;
    }

    public function recipient($newRecipient = null): string
    {
        if (is_string($newRecipient) && $newRecipient != '') {
            $this->recipient = $newRecipient;
        }
        return $this->recipient;
    }
}
