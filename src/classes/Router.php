<?php
namespace Classes;

class Router
{
  private $uri;
  private $baseUrl;

  const ROUTES = [
    '/' => 'index',
    '/cart' => 'cart',
    '/checkout' => 'checkout',
    '/checkoutsuccess' => 'checkoutSuccess',
    '/checkoutfailure' => 'checkoutFailure'
  ];

  public function __construct(string $uri, Session $session)
  {
    //Prepares $uri to store the arguments
    $uri = explode('/', $uri);
    $this->baseUrl = "/{$uri[1]}";
    if (empty(self::ROUTES[$this->baseUrl])) {
      $this->redirect('/');
    }
    $this->uri = $uri;
    $this->session = $session;
  }

  protected function redirect(string $uri = "/"): void
  {
    header("Location: {$uri}");
  }

  public function show(): void
  {
    $this->{self::ROUTES[$this->baseUrl]}();
  }

  public function index(): void
  {
    if ($this->session->isStarted() && !$this->session->contains('cart')) {
      $this->session->set('cart', Cart::build([], $this->session));
    }
    $marketplace = new Marketplace();
    $fantasies = $marketplace->getProducts([]);
    include("templates/index.php");
  }

  public function cart(): void
  {
    $cart = Cart::Build([], $this->session);
    $marketplace = new \Classes\Marketplace();
    $actions = [
      'add' => function (\Classes\Marketplace $marketplace, \Classes\Cart $cart) {
        $fantasies = $marketplace->getProducts([$this->uri[3]]);
        $cart->add($fantasies[0]);
        $this->redirect($this->baseUrl);
      },
      'remove' => function (\Classes\Marketplace $marketplace, \Classes\Cart $cart) {
        $cart->remove($this->uri[3]);
        $this->redirect($this->baseUrl);
      },
      'process' => function (\Classes\Marketplace $marketplace, \Classes\Cart $cart) {
        $args = [
          'amount' => [
            'filter' => FILTER_VALIDATE_INT,
            'flags' => FILTER_REQUIRE_ARRAY
          ],
          'redirect' => FILTER_SANITIZE_STRING
        ];
        $cartPost = filter_input_array(INPUT_POST, $args);
        $cart->recalc($cartPost['amount']);
        $this->redirect("/{$cartPost['redirect']}");
      }
    ];

    if (isset($actions[$this->uri[2]])) {
      $actions[$this->uri[2]]($marketplace, $cart);
    }
    if ($cart->totalProducts() > 0) {
      $fare = $marketplace::FARE;
    }

    include("templates/cart.php");
  }

  public function checkout(): void
  {
    $marketplace = new Marketplace();
    $fare = $marketplace::FARE;
    $cart = Cart::Build([], $this->session);
    $daoObj = $this->dao = new DAOJson('pagarme');
    $pagarMeKeys = $this->dao->get('key', [])[0];
    $pagarmeAdapter = new \Thirdparties\PagarmeAdapter($marketplace, $cart, $pagarMeKeys);

    if (count($_POST) > 0 && isset($_POST['token']) && is_string($_POST['token'])) {
      $args = [
        'token' => FILTER_SANITIZE_STRING,
        'payment_method' => FILTER_SANITIZE_STRING
      ];
      $pagarmePost = filter_input_array(INPUT_POST, $args);
      $serverResult = $pagarmeAdapter->splitPayment($pagarmePost);
      if (get_class($serverResult) === 'PagarMe\Sdk\Transaction\CreditCardTransaction') {
        $this->redirect('/checkoutsuccess');
      } else {
        $this->redirect('/checkoutfailure');
      }
    }

    include("templates/checkout.php");
  }

  public function checkoutSuccess(): void
  {
    $cart = Cart::Build([], $this->session);
    $cart->clear();
    $msg = "Pagamento concluído, muito obrigado!";
    $status='success';
    include("templates/msg.php");
  }

  public function checkoutFailure(): void
  {
    $msg = "Houve uma falha na transação, por favor, tente novamente!";
    $status = 'failure';
    include("templates/msg.php");
  }
}
