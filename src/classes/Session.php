<?php
namespace Classes;

class Session
{
    private $status = false;
    public function __construct()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
            $this->status = true;
        }
    }

    public function isStarted(): bool
    {
      return $this->status;
    }

    private function checkInstance(): void
    {
      if (!$this->status) {
        throw new \BadFunctionCallException(
          "Can't use Session routines without a started session", 500
        );
      }
    }

    public function destroy(): void
    {
        $this->checkInstance();
        session_destroy();
    }

    public function get(string $name, $default = null)
    {
      $this->checkInstance();
      return isset($_SESSION[$name]) ?
        $_SESSION[$name] : $default;
    }

    public function set(string $name, $value = null): bool
    {
      $this->checkInstance();
      $_SESSION[$name] = $value;

      return isset($_SESSION[$name]);
    }

    public function contains(string $name): bool
    {
      $this->checkInstance();

      return isset($_SESSION[$name]);
    }

    public function unset(string $name): bool
    {
      $this->checkInstance();
      unset($_SESSION[$name]);

      return !isset($_SESSION[$name]);
    }
}
