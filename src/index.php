<?php
    require __DIR__ . '/vendor/autoload.php';
    $session = new Classes\Session();
    $objRouter = new Classes\Router($_SERVER['REQUEST_URI'], $session);
    ob_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Fantasy Market</title>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link rel="stylesheet" href="/assets/css/style.css" type="text/css" media="screen">
	<link rel="shortcut icon" type="image/png" href="/favicon.png"/>
	<nav class="menu">
	  <a href='/' class='fas fa-theater-masks' id='logo'><span>Fantasy Market</span></a>
      <a href='cart' class='fas fa-shopping-cart' id='cart'><span><?= $session->get('cart')->totalProducts(); ?></span></a>
	</nav>
    </head>
    <body>
	<?php
        $objRouter->show();
    ?>
    </body>
</html>
<?php ob_end_flush(); ?>
