<div class='container' id='cart-container'>
  <a href='/' class='btn fas fa-arrow-left' id='cart-back'>Voltar às compras</a>
  <form action="cart/process" method="POST" id="cart-form" >
    <table style="width:100%">
    <thead>
      <th>PRODUTO</th>
      <th>FORNECEDOR</th>
      <th>QUANTIDADE</th>
      <th>VALOR UNIT.</th>
      <th>VALOR</th>
      <th>AÇÕES</th>
    </thead>
    <?php if (isset($fare)): ?>
    <tbody>
    <?php foreach ($cart->products() as $product): ?>
      <tr>
        <td><?= $product->name(); ?></td>
        <td><?= $product->provider()->name(); ?></td>
        <td>
          <input type='number' name='amount[<?= $product->id(); ?>]' value='<?= $product->amount(); ?>'>
        </td>
        <td><?= $product->value(null, true); ?></td>
        <td><?= $marketplace::formatNumber($product->totalValue()); ?></td>
        <td>
        <a href='cart/remove/<?= $product->id() ?>' class='btn fas fa-trash'>Remover</a>
        </td>
      </tr>
    <?php endforeach; ?>
    </tbody>
    <tfoot>
      <tr>
        <td colspan='4'>
          SubTotal:
        </td>
        <td>
          <?= $cart->totalValue(true); ?>
        </td>
        <td>
        </td>
      </tr>
      <tr>
        <td colspan='4'>
          Frete:
        </td>
        <td>
          <?= $marketplace::formatNumber($fare); ?>
        </td>
        <td>
        </td>
      </tr>
      <tr>
        <td colspan='4'>
          Total:
        </td>
        <td>
          <?= $marketplace::formatNumber($cart->totalValue(false) + $fare) ; ?>
        </td>
        <td>
        </td>
      </tr>
    </tfoot>
    <?php else: ?>
    <tbody>
      <tr id="empty-cart">
        <td colspan='6'><i class='fas fa-sad-cry'></i>O carrinho está vazio</td>
      </tr>
    </tbody>
    <?php endif; ?>
    </table> 
  </form>
  <?php if (isset($fare)): ?>
  <div class='flexbox flex-end'>
    <button type="submit" class='btn fas fa-sync' form="cart-form" name='redirect' value="cart">Recalcular</button>
    <button type="submit" class='btn fas fa-money-bill' form="cart-form" name='redirect' value="checkout">Finalizar Compra</button>
  </div>
  <?php endif; ?>
</div>
