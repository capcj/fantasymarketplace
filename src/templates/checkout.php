<div class='container' id='checkout-container'>
  <h1>Checkout</h1>
    <table style="width:100%">
    <thead>
      <th>PRODUTO</th>
      <th>FORNECEDOR</th>
      <th>QUANTIDADE</th>
      <th>VALOR UNIT.</th>
      <th>VALOR</th>
    </thead>
    <tbody>
    <?php foreach ($cart->products() as $product): ?>
      <tr>
        <td><?= $product->name(); ?></td>
        <td><?= $product->provider()->name(); ?></td>
        <td>
          <?= $product->amount(); ?>
        </td>
        <td><?= $product->value(null, true); ?></td>
        <td><?= $marketplace::formatNumber($product->totalValue()); ?></td>
      </tr>
    <?php endforeach; ?>
    </tbody>
    <tfoot>
      <?php if (isset($fare)): ?>
      <tr>
        <td colspan='3'>
          SubTotal:
        </td>
        <td>
          <?= $cart->totalValue(true); ?>
        </td>
        <td>
        </td>
      </tr>
      <tr>
        <td colspan='3'>
          Frete:
        </td>
        <td>
          <?= $marketplace::formatNumber($fare); ?>
        </td>
        <td>
        </td>
      </tr>
      <tr>
        <td colspan='3'>
          Total:
        </td>
        <td>
          <?= $marketplace::formatNumber($cart->totalValue(false) + $fare) ; ?>
        </td>
        <td>
        </td>
      </tr>
      <?php endif; ?>
    </tfoot>
    </table> 
  <form method="POST">
      <script type="text/javascript"
          src="https://assets.pagar.me/checkout/checkout.js"
          data-encryption-key="<?= $pagarmeAdapter->encryptionKey(); ?>"
          data-customer-data="true"
          data-create-token="true"
          data-payment-methods="credit_card"
          data-amount="<?= $pagarmeAdapter->totalAmount(); ?>">
      </script>
  </form>
</div>
