<div class='container'>
	<form action="PUT" id='fantasies-container'>
	<?php
        if (count($fantasies) > 0) {
            foreach ($fantasies as $fantasy) {
                echo "<div class='fantasy' id='fantasy-{$fantasy->id(null)}'>",
                "<i class='fas fas-lg fa-theater-masks'></i>",
                "<p>{$fantasy->name(null)}</p>",
                "<p>Fornecido por {$fantasy->provider(null)->name(null)}</p>",
                "<p>{$fantasy->value(null, true)}</p>",
                "<a href='cart/add/{$fantasy->id(null)}' class='btn fas fa-cart-arrow-down'>Comprar</a>",
                "</div>";
            }
        }
    ?>
	</form>
</div>
