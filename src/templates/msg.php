<div class='container <?= $status; ?>' id='container-msg'>
  <i class="fas <?= $status == 'success' ? 'fa-check-circle' : 'fa-sad-tear'; ?>"></i>
  <?= $msg; ?> 
</div>
