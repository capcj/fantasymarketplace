<?php
namespace Thirdparties;

class PagarmeAdapter
{
    private $cart;
    private $marketplace;
    private $pagarmeObj;
    private $transaction;
    private $ownerRecipient;
    private $keys;

    public function __construct(\Classes\Marketplace $marketplace, \Classes\Cart $cart, \Classes\Key $keys)
    {
        $this->cart = $cart;
        $this->marketplace = $marketplace;
        $this->keys = $keys;
        $this->pagarmeObj = new \PagarMe\Sdk\PagarMe($this->keys->api());
    }

    public function convertAmount($amount): int
    {
        return (int) $amount * 100;
    }

    public function encryptionKey(): string
    {
        return $this->keys->encryption();
    }

    public function totalAmount()
    {
        return $this->convertAmount(
      $this->cart->totalValue(false) + $this->marketplace::FARE
    );
    }

    public function getRecipient(string $id): \PagarMe\Sdk\Recipient\Recipient
    {
        return $this->pagarmeObj->recipient()->get($id);
    }

    private function calcSplitRulesAmount(): array
    {
        $splitRules = $this->marketplace->getSplitRules();
        $valuesPerRecipient = $this->cart->valuesPerProviderRecipient();
        $fare = $this->marketplace::FARE;
        $totalRecipients = count($valuesPerRecipient);
        $splitFare = (int) $fare / $totalRecipients;
        $fareGiven = 0;
        $recipientsGiven = 0;
        $this->ownerRecipient = $this->marketplace->getOwnerRecipient($this->cart);
        $result = [];

        foreach ($this->cart->providers() as $provider) {
            if ($recipientsGiven == $totalRecipients - 1) {
                $splitFare = $fare - $fareGiven;
            }
            $ruledAmount =
      $splitRules[$provider->owner()]($valuesPerRecipient[$provider->recipient()], $splitFare);

            if (is_array($ruledAmount)) {
                $result[$this->ownerRecipient] += $ruledAmount[1];
                $result[$provider->recipient()] += $ruledAmount[0];
            } else {
                $result[$provider->recipient()] += $ruledAmount;
            }

            $fareGiven += $splitFare;
            $recipientsGiven++;
        }

        return $result;
    }

    public function splitPayment(array $transactionArr): \PagarMe\Sdk\Transaction\CreditCardTransaction
    {
        $this->transaction = $this->pagarmeObj->transaction()->get($transactionArr['token']);
        $splitrulesColl = new \PagarMe\Sdk\SplitRule\SplitRuleCollection();
        $splitRulesAmount = $this->calcSplitRulesAmount();

        $getRecipientsId = function ($recipients, $recipient) {
            $recipients[$recipient] = $this->pagarmeObj->recipient()->get($recipient);
            return $recipients;
        };

        $recipients = array_reduce(array_keys($splitRulesAmount), $getRecipientsId);
        $totalRecipients = count($recipients);
        $adapter = $this;

        $generateSplitRulesArr = function (array $splitRulesAmount) use ($recipients, $adapter): array {
            $splitRules = [];
            //Owner always will be first
            $chargeProcessingFee = true;
            $liable = true;
            foreach ($splitRulesAmount as $providerRecipient => $amount) {
                $splitRules[] = $this->pagarmeObj->splitRule()->monetaryRule(
            $adapter->convertAmount($amount),
            $recipients[$providerRecipient],
            $chargeProcessingFee,
            $liable
          );
                $chargeProcessingFee = false;
                $liable = false;
            }

            return $splitRules;
        };

        $splitRulesArr = $generateSplitRulesArr($splitRulesAmount);

        for ($i=0;$i < $totalRecipients; $i++) {
            $splitrulesColl[$i] = $splitRulesArr[$i];
        }

        return $this->pagarmeObj->transaction()->capture(
      $this->transaction,
        $this->totalAmount(),
        [],
        $splitrulesColl
    );
    }
}
